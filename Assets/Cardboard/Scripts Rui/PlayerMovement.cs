﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;
    [SerializeField] private float rotationSpeed;

    [SerializeField] private float mouseSensitivity;
    [SerializeField] private float clampAngleY;

    private Vector3 moveInput;
    private Vector2 rotationInput;
    private float speed;
    private float rotX = 0.0f; // rotation around the right/x axis
    private float rotY = 0.0f; // rotation around the up/y axis

    private void Awake()
    {
        Cursor.visible = false;
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
    }

    private void Update()
    {
        GetMoveInput();
        Move();

        GetRotationInput();
        Rotate();
    }

    private void GetMoveInput()
    {
        moveInput.x = Input.GetAxis("Horizontal");
        moveInput.z = Input.GetAxis("Vertical");
    }

    private void Move()
    {
        // If player isn't moving, no need to go further
        if (moveInput == Vector3.zero) return;

        speed = (Input.GetKey(KeyCode.LeftShift)) ? runSpeed : walkSpeed;

        transform.position += moveInput * speed * Time.deltaTime;
    }

    private void GetRotationInput()
    {
        rotationInput.x = Input.GetAxis("Mouse X");
        rotationInput.y = -Input.GetAxis("Mouse Y");
    }

    private void Rotate()
    {
        rotY += rotationInput.x * 300 * Time.deltaTime;
        rotX += rotationInput.y * 300 * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -clampAngleY, clampAngleY);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;
    }
}
