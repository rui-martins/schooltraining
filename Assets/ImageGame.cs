﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ImageGame : MonoBehaviour {
	public RawImage imagem;
	public ImagemDeJogo[] jogos;
	public Text opcao1;
	public Text opcao2;
	int opcaoSelecionada;
	int jogoAtual = 0;


}

[System.Serializable]
public class ImagemDeJogo
{
	
	public Texture texturaDaImagem;
	public string[] opcoes;
}
