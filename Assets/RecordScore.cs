﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordScore : MonoBehaviour {
	GameObject Letters;
	public Text Print;
	bool right = false;
	bool ifRestart = false;
	int numGame = 0;
	int score;
	// Use this for initialization
	void Start () {
		Letters = GameObject.Find ("Letters");
		Print.text = "Score/ Game Played: " + score.ToString () + "/ " + numGame.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.L)){
			if (!ifRestart) {
				numGame++;
				//Debug.Log ("game++");
				Letters = GameObject.Find ("Letters");
				right = Letters.GetComponent<chooseLetter> ().right;
				//Debug.Log (Letters.GetComponent<chooseLetter> ().right);
				if (right) {
					//Debug.Log ("get score");
					score++;
				}
				Print.text = "Score/ Game Played: " + score.ToString () + "/ " + numGame.ToString ();
			}
			ifRestart = !ifRestart;
		}

	}
}
