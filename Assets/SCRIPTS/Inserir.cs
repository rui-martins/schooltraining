﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Inserir : MonoBehaviour {

	public Text texto;
	public Button btn;
	public  GameObject userInterface;
	public GameObject vrPlayer;
	public GameObject cm;
	public GameObject Caixa1, Caixa2, Caixa3, Caixa4, Caixa5, Caixa6, Caixa7, Caixa8, Caixa9, Caixa10, Caixa11, Caixa12, Caixa13, Caixa14, Caixa15, Caixa16, Caixa17, Caixa18, Caixa19, Caixa20;
	public GameObject A1, B1, C1, D1, E1, F1, G1, H1, I1, J1, K1, L1, M1, N1, O1, P1, Q1, R1, S1, T1, U1, V1, W1, X1, Y1, Z1, Ç1;
	public GameObject A2, B2, C2, D2, E2, F2, G2, H2, I2, J2, K2, L2, M2, N2, O2, P2, Q2, R2, S2, T2, U2, V2, W2, X2, Y2, Z2, Ç2;
	public GameObject A3, B3, C3, D3, E3, F3, G3, H3, I3, J3, K3, L3, M3, N3, O3, P3, Q3, R3, S3, T3, U3, V3, W3, X3, Y3, Z3, Ç3;
	public GameObject A4, B4, C4, D4, E4, F4, G4, H4, I4, J4, K4, L4, M4, N4, O4, P4, Q4, R4, S4, T4, U4, V4, W4, X4, Y4, Z4, Ç4;
	public GameObject A5, B5, C5, D5, E5, F5, G5, H5, I5, J5, K5, L5, M5, N5, O5, P5, Q5, R5, S5, T5, U5, V5, W5, X5, Y5, Z5, Ç5;
	public GameObject A6, B6, C6, D6, E6, F6, G6, H6, I6, J6, K6, L6, M6, N6, O6, P6, Q6, R6, S6, T6, U6, V6, W6, X6, Y6, Z6, Ç6;
	public GameObject A7, B7, C7, D7, E7, F7, G7, H7, I7, J7, K7, L7, M7, N7, O7, P7, Q7, R7, S7, T7, U7, V7, W7, X7, Y7, Z7, Ç7;
	public GameObject A8, B8, C8, D8, E8, F8, G8, H8, I8, J8, K8, L8, M8, N8, O8, P8, Q8, R8, S8, T8, U8, V8, W8, X8, Y8, Z8, Ç8;
	public GameObject A9, B9, C9, D9, E9, F9, G9, H9, I9, J9, K9, L9, M9, N9, O9, P9, Q9, R9, S9, T9, U9, V9, W9, X9, Y9, Z9, Ç9;
	public GameObject A10, B10, C10, D10, E10, F10, G10, H10, I10, J10, K10, L10, M10, N10, O10, P10, Q10, R10, S10, T10, U10, V10, W10, X10, Y10, Z10, Ç10;
	public GameObject A11, B11, C11, D11, E11, F11, G11, H11, I11, J11, K11, L11, M11, N11, O11, P11, Q11, R11, S11, T11, U11, V11, W11, X11, Y11, Z11, Ç11;
	public GameObject A12, B12, C12, D12, E12, F12, G12, H12, I12, J12, K12, L12, M12, N12, O12, P12, Q12, R12, S12, T12, U12, V12, W12, X12, Y12, Z12, Ç12;
	public GameObject A13, B13, C13, D13, E13, F13, G13, H13, I13, J13, K13, L13, M13, N13, O13, P13, Q13, R13, S13, T13, U13, V13, W13, X13, Y13, Z13, Ç13;	
	public GameObject A14, B14, C14, D14, E14, F14, G14, H14, I14, J14, K14, L14, M14, N14, O14, P14, Q14, R14, S14, T14, U14, V14, W14, X14, Y14, Z14, Ç14;	
	public GameObject A15, B15, C15, D15, E15, F15, G15, H15, I15, J15, K15, L15, M15, N15, O15, P15, Q15, R15, S15, T15, U15, V15, W15, X15, Y15, Z15, Ç15;	
	public GameObject A16, B16, C16, D16, E16, F16, G16, H16, I16, J16, K16, L16, M16, N16, O16, P16, Q16, R16, S16, T16, U16, V16, W16, X16, Y16, Z16, Ç16;	
	public GameObject A17, B17, C17, D17, E17, F17, G17, H17, I17, J17, K17, L17, M17, N17, O17, P17, Q17, R17, S17, T17, U17, V17, W17, X17, Y17, Z17, Ç17;	
	public GameObject A18, B18, C18, D18, E18, F18, G18, H18, I18, J18, K18, L18, M18, N18, O18, P18, Q18, R18, S18, T18, U18, V18, W18, X18, Y18, Z18, Ç18;	
	public GameObject A19, B19, C19, D19, E19, F19, G19, H19, I19, J19, K19, L19, M19, N19, O19, P19, Q19, R19, S19, T19, U19, V19, W19, X19, Y19, Z19, Ç19;	
	public GameObject A20, B20, C20, D20, E20, F20, G20, H20, I20, J20, K20, L20, M20, N20, O20, P20, Q20, R20, S20, T20, U20, V20, W20, X20, Y20, Z20, Ç20;	
	public GameObject V, F, mensagem_final, ganhou, Keyboard;
	public GameObject Cabeca, Torco, Braco1, Braco2, Perna1, Perna2;
	public int contador=0,cont=0,missprimeira=0,misssegunda=0, score=0, score2=0, total=0;
	private bool acertou=false, falhou=false;

	public int tamanho;
	public char[] word;
	public GameObject textoerro, textoerroo;
	private string str;
	public string[] words;
	public string palavra;
	private GameObject canvas;
	private string letra;

	void TaskOnClick()
	{
		//contador = 0;
		palavra = texto.text.ToString(); 
		tamanho = palavra.Replace(" ", "").Length;
		cm.SetActive(false);
		//GameObject.Find("cm").transform.localScale= new Vector3(0,0,0);

		vrPlayer.SetActive(true);

		char[] charArr = palavra.ToCharArray();
		for (int i = 0; charArr[i] != ' '; i++)
		{

			words = palavra.Split(' ');
		}

		if (words.Length > 2) {
			textoerro.SetActive (true);
		} else if (words [0].Length > 10 || words [1].Length > 10) {
			textoerroo.SetActive (true);
		}
		else
		{
			userInterface.SetActive(false);
		}
		foreach (string parte in words)
		{

			if (words[0].Length == 1)
			{
				Caixa1.SetActive(true);

			}
			if (words[0].Length == 2)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
			}
			if (words[0].Length == 3)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
				Caixa3.SetActive(true);
			}
			if (words[0].Length == 4)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
				Caixa3.SetActive(true);
				Caixa4.SetActive(true);
			}
			if (words[0].Length == 5)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
				Caixa3.SetActive(true);
				Caixa4.SetActive(true);
				Caixa5.SetActive(true);
			}
			if (words[0].Length == 6)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
				Caixa3.SetActive(true);
				Caixa4.SetActive(true);
				Caixa5.SetActive(true);
				Caixa6.SetActive(true);
			}
			if (words[0].Length == 7)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
				Caixa3.SetActive(true);
				Caixa4.SetActive(true);
				Caixa5.SetActive(true);
				Caixa6.SetActive(true);
				Caixa7.SetActive(true);
			}
			if (words[0].Length == 8)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
				Caixa3.SetActive(true);
				Caixa4.SetActive(true);
				Caixa5.SetActive(true);
				Caixa6.SetActive(true);
				Caixa7.SetActive(true);
				Caixa8.SetActive(true);
			}
			if (words[0].Length == 9)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
				Caixa3.SetActive(true);
				Caixa4.SetActive(true);
				Caixa5.SetActive(true);
				Caixa6.SetActive(true);
				Caixa7.SetActive(true);
				Caixa8.SetActive(true);
				Caixa9.SetActive(true);
			}
			if (words[0].Length == 10)
			{
				Caixa1.SetActive(true);
				Caixa2.SetActive(true);
				Caixa3.SetActive(true);
				Caixa4.SetActive(true);
				Caixa5.SetActive(true);
				Caixa6.SetActive(true);
				Caixa7.SetActive(true);
				Caixa8.SetActive(true);
				Caixa9.SetActive(true);
				Caixa10.SetActive(true);

			}
			if (words[1].Length == 1)
			{

				Caixa11.SetActive(true);

			}
			if (words[1].Length == 2)
			{

				Caixa11.SetActive(true);
				Caixa12.SetActive(true);

			}
			if (words[1].Length == 3)
			{

				Caixa11.SetActive(true);
				Caixa12.SetActive(true);
				Caixa13.SetActive(true);

			}
			if (words[1].Length == 4)
			{

				Caixa11.SetActive(true);
				Caixa12.SetActive(true);
				Caixa13.SetActive(true);
				Caixa14.SetActive(true);

			}
			if (words[1].Length == 5)
			{

				Caixa11.SetActive(true);
				Caixa12.SetActive(true);
				Caixa13.SetActive(true);
				Caixa14.SetActive(true);
				Caixa15.SetActive(true);
			}
			if (words[1].Length == 6)
			{

				Caixa11.SetActive(true);
				Caixa12.SetActive(true);
				Caixa13.SetActive(true);
				Caixa14.SetActive(true);
				Caixa15.SetActive(true);
				Caixa16.SetActive(true);

			}
			if (words[1].Length == 7)
			{


				Caixa11.SetActive(true);
				Caixa12.SetActive(true);
				Caixa13.SetActive(true);
				Caixa14.SetActive(true);
				Caixa15.SetActive(true);
				Caixa16.SetActive(true);
				Caixa17.SetActive(true);

			}
			if (words[1].Length == 8)
			{

				Caixa11.SetActive(true);
				Caixa12.SetActive(true);
				Caixa13.SetActive(true);
				Caixa14.SetActive(true);
				Caixa15.SetActive(true);
				Caixa16.SetActive(true);
				Caixa17.SetActive(true);
				Caixa18.SetActive(true);
			}
			if (words[1].Length == 9)
			{

				Caixa11.SetActive(true);
				Caixa12.SetActive(true);
				Caixa13.SetActive(true);
				Caixa14.SetActive(true);
				Caixa15.SetActive(true);
				Caixa16.SetActive(true);
				Caixa17.SetActive(true);
				Caixa18.SetActive(true);
				Caixa19.SetActive(true);
			}
			if (words[1].Length == 10)
			{

				Caixa11.SetActive(true);
				Caixa12.SetActive(true);
				Caixa13.SetActive(true);
				Caixa14.SetActive(true);
				Caixa15.SetActive(true);
				Caixa16.SetActive(true);
				Caixa17.SetActive(true);
				Caixa18.SetActive(true);
				Caixa19.SetActive(true);
				Caixa20.SetActive(true);
			}
		}
	}

	void Start () {
		
	}

	void Update () {
		Button botao = btn.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
		RaycastHit hitInfo = new RaycastHit();
		bool hit = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo); 
		if (hit)
		{
			

			if (Input.GetKeyDown(KeyCode.Joystick1Button0)|| Input.GetKeyDown(KeyCode.K)){
				letralida(hitInfo.transform.gameObject.name);

				if (acertou = true) {
					StartCoroutine (Acertou ());
				} else{
					StartCoroutine (Falhou ());
				}
			}
		}

	}

	IEnumerator Acertou(){
		V.SetActive (true);
		yield return new WaitForSeconds(5f);
		V.SetActive (false);
		acertou = false;
	}

	IEnumerator Falhou(){
		F.SetActive (true);
		yield return new WaitForSeconds(5f);
		F.SetActive (false);
		falhou = false;
	}




	void letralida(string letra){

		string letra_escolhida;
		if (letra == "q" || letra == "w" || letra == "e" || letra == "r" || letra == "t" || letra == "y" || letra == "u" || letra == "i" || letra == "o" || letra == "p" || letra == "a" || letra == "s" || letra == "d" || letra == "f" || letra == "g" || letra == "h" || letra == "j" || letra == "k" || letra == "l" || letra == "ç" || letra == "z" || letra == "x" || letra == "c" || letra == "v" || letra == "b" || letra == "n" || letra == "m" || letra==" ") { 
			if (letra == " ") {
				Debug.Log ("Não é uma letra!");
			} else {
				Debug.Log ("letra enviada " + letra);
				compare(letra,words);
			}	
		} else {
			Debug.Log ("Não é uma letra!");
		}

	}

	void compare(string letraselecionada, string[] words){
		char[] charletraselecionada = letraselecionada.ToCharArray ();
		string palavra1 = words [0];
		string palavra2 = words [1];

		char[] palavra12 = palavra1.ToCharArray ();
		char[] palavra22 = palavra2.ToCharArray ();
		Debug.Log ("palavra1 " + palavra1);
		Debug.Log ("palavra2 " + palavra2);
		total = palavra12.Length + palavra22.Length;
		contador = 0;
		cont = 0;
		misssegunda = 0;
		missprimeira = 0;
		for (int i = 0; i < palavra12.Length; i++) {
			if (charletraselecionada [0] != palavra12 [i]) {

				contador++;
				Debug.Log ("numero de erros 1 palavra " + contador);
			}
		}

		if (contador == palavra12.Length) {
			missprimeira = 1;
			/*contador = 0;
		} else {
			contador = 0;*/
		}

		for (int x = 0; x < palavra22.Length; x++) {
			if (charletraselecionada [0] != palavra22 [x]) {

				cont++;
				Debug.Log ("numero de erros 2 palavra " + cont);
			}
		}

		if (cont == palavra22.Length) {
			misssegunda = 1;
			/*cont = 0;
		}else {
			cont = 0;*/
		}

		if (misssegunda == 1 && missprimeira == 1) {
			Debug.Log ("Ffsdhfodsjfnfjdsnfjsd ");
			falhou = true;
			Falhou ();
			//Cabeca, Torco, Braco1, Braco2, Perna1, Perna2;
			if (Cabeca.activeSelf) {
				if (Torco.activeSelf) {
					if (Braco1.activeSelf) {
						if (Braco2.activeSelf) {
							if (Perna1.activeSelf) {
								if (Perna2.activeSelf) {
									//dead
								} else {
									Perna2.SetActive (true);
									Debug.Log ("morreste");
									mensagem_final.SetActive (true);
									Keyboard.SetActive(false);
								}
							} else {
								Perna1.SetActive (true);
							}
						} else {
							Braco2.SetActive (true);
						}
					} else {
						Braco1.SetActive (true);
					}
				} else {
					Torco.SetActive (true);
				}
			} else {
				Cabeca.SetActive (true);
			}

		}



		for (int i = 0; i < palavra12.Length; i++) {
			if (charletraselecionada [0] == palavra12 [i]) {
				Debug.Log ("palavra " + palavra12 [0]);
				acertou = true;
				Acertou ();


				if (i == 0) {
					string palavrateste = palavra12 [0].ToString ();
					if (palavrateste.Equals ("a")) {
						A1.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B1.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C1.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D1.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E1.SetActive (true);
						Debug.Log ("passou aqui E1");

					} else if (palavrateste.Equals ("f")) {
						F1.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G1.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H1.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I1.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J1.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K1.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L1.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M1.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N1.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O1.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P1.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q1.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R1.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S1.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T1.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U1.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V1.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W1.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X1.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y1.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z1.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç1.SetActive (true);

					}
				}
				else if (i == 1) {
					Debug.Log ("palavra " + palavra12 [1]);
					string palavrateste = palavra12 [1].ToString ();
					if (palavrateste.Equals ("a")) {
						A2.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B2.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C2.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D2.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E2.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F2.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G2.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H2.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I2.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J2.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K2.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L2.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M2.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N2.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O2.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P2.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q2.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R2.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S2.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T2.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U2.SetActive (true);
						Debug.Log ("passou aqui U2");

					} else if (palavrateste.Equals ("v")) {
						V2.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W2.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X2.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y2.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z2.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç2.SetActive (true);

					}
				}
				else if (i == 2) {
					Debug.Log ("palavra " + palavra12 [2]);
					string palavrateste = palavra12 [2].ToString ();
					if (palavrateste.Equals ("a")) {
						A3.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B3.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C3.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D3.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E3.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F3.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G3.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H3.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I3.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J3.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K3.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L3.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M3.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N3.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O3.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P3.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q3.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R3.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S3.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T3.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U3.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V3.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W3.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X3.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y3.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z3.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç3.SetActive (true);

					}
				}
				else if (i == 3) {
					Debug.Log ("palavra " + palavra12 [3]);
					string palavrateste = palavra12 [3].ToString ();
					if (palavrateste.Equals ("a")) {
						A4.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B4.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C4.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D4.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E4.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F4.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G4.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H4.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I4.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J4.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K4.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L4.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M4.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N4.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O4.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P4.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q4.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R4.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S4.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T4.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U4.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V4.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W4.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X4.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y4.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z4.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç4.SetActive (true);

					}
				}
				else if (i == 4) {
					Debug.Log ("palavra " + palavra12 [4]);
					string palavrateste = palavra12 [4].ToString ();
					if (palavrateste.Equals ("a")) {
						A5.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B5.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C5.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D5.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E5.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F5.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G5.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H5.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I5.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J5.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K5.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L5.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M5.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N5.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O5.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P5.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q5.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R5.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S5.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T5.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U5.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V5.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W5.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X5.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y5.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z5.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç5.SetActive (true);

					}
				}
				else if (i == 5) {
					Debug.Log ("palavra " + palavra12 [5]);
					string palavrateste = palavra12 [5].ToString ();
					if (palavrateste.Equals ("a")) {
						A6.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B6.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C6.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D6.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E6.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F6.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G6.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H6.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I6.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J6.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K6.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L6.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M6.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N6.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O6.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P6.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q6.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R6.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S6.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T6.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U6.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V6.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W6.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X6.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y6.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z6.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç6.SetActive (true);

					}
				}
				else if (i == 6) {
					Debug.Log ("palavra " + palavra12 [6]);
					string palavrateste = palavra12 [6].ToString ();
					if (palavrateste.Equals ("a")) {
						A7.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B7.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C7.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D7.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E7.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F7.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G7.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H7.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I7.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J7.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K7.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L7.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M7.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N7.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O7.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P7.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q7.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R7.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S7.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T7.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U7.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V7.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W7.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X7.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y7.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z7.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç7.SetActive (true);

					}
				}
				else if (i == 7) {
					Debug.Log ("palavra " + palavra12 [7]);
					string palavrateste = palavra12 [7].ToString ();
					if (palavrateste.Equals ("a")) {
						A8.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B8.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C8.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D8.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E8.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F8.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G8.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H8.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I8.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J8.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K8.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L8.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M8.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N8.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O8.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P8.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q8.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R8.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S8.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T8.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U8.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V8.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W8.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X8.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y8.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z8.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç8.SetActive (true);

					}
				}
				else if (i == 8) {
					Debug.Log ("palavra " + palavra12 [8]);
					string palavrateste = palavra12 [8].ToString ();
					if (palavrateste.Equals ("a")) {
						A9.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B9.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C9.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D9.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E9.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F9.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G9.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H9.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I9.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J9.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K9.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L9.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M9.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N9.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O9.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P9.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q9.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R9.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S9.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T9.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U9.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V9.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W9.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X9.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y9.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z9.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç9.SetActive (true);

					}
				}
				else if (i == 9) {
					Debug.Log ("palavra " + palavra12 [9]);
					string palavrateste = palavra12 [9].ToString ();
					if (palavrateste.Equals ("a")) {
						A10.SetActive (true);

					} else if (palavrateste.Equals ("b")) {
						B10.SetActive (true);

					} else if (palavrateste.Equals ("c")) {
						C10.SetActive (true);

					} else if (palavrateste.Equals ("d")) {
						D10.SetActive (true);

					} else if (palavrateste.Equals ("e")) {
						E10.SetActive (true);

					} else if (palavrateste.Equals ("f")) {
						F10.SetActive (true);

					} else if (palavrateste.Equals ("g")) {
						G10.SetActive (true);

					} else if (palavrateste.Equals ("h")) {
						H10.SetActive (true);

					} else if (palavrateste.Equals ("i")) {
						I10.SetActive (true);

					} else if (palavrateste.Equals ("j")) {
						J10.SetActive (true);

					} else if (palavrateste.Equals ("k")) {
						K10.SetActive (true);

					} else if (palavrateste.Equals ("l")) {
						L10.SetActive (true);

					} else if (palavrateste.Equals ("m")) {
						M10.SetActive (true);

					} else if (palavrateste.Equals ("n")) {
						N10.SetActive (true);

					} else if (palavrateste.Equals ("o")) {
						O10.SetActive (true);

					} else if (palavrateste.Equals ("p")) {
						P10.SetActive (true);

					} else if (palavrateste.Equals ("q")) {
						Q10.SetActive (true);

					} else if (palavrateste.Equals ("r")) {
						R10.SetActive (true);

					} else if (palavrateste.Equals ("s")) {
						S10.SetActive (true);

					} else if (palavrateste.Equals ("t")) {
						T10.SetActive (true);

					} else if (palavrateste.Equals ("u")) {
						U10.SetActive (true);

					} else if (palavrateste.Equals ("v")) {
						V10.SetActive (true);

					} else if (palavrateste.Equals ("w")) {
						W10.SetActive (true);

					} else if (palavrateste.Equals ("x")) {
						X10.SetActive (true);

					} else if (palavrateste.Equals ("y")) {
						Y10.SetActive (true);

					} else if (palavrateste.Equals ("z")) {
						Z10.SetActive (true);

					} else if (palavrateste.Equals ("ç")) {
						Ç10.SetActive (true);

					}
				} 

			}
			for (int x = 0; x < palavra22.Length; x++) {
				if (charletraselecionada [0] == palavra22 [x]) {
					Acertou ();
					if (x == 0) {
						string palavrateste = palavra22 [0].ToString ();
						if (palavrateste.Equals ("a")) {
							A11.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B11.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C11.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D11.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E11.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F11.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G11.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H11.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I11.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J11.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K11.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L11.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M11.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N11.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O11.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P11.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q11.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R11.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S11.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T11.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U11.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V11.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W11.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X11.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y11.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z11.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç11.SetActive (true);

						}

					} else if (x == 1) {
						string palavrateste = palavra22 [1].ToString ();
						if (palavrateste.Equals ("a")) {
							A12.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B12.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C12.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D12.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E12.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F12.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G12.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H12.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I12.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J12.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K12.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L12.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M12.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N12.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O12.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P12.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q12.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R12.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S12.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T12.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U12.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V12.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W12.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X12.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y12.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z12.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç12.SetActive (true);

						}


					} else if (x == 2) {

						string palavrateste = palavra22 [2].ToString ();
						if (palavrateste.Equals ("a")) {
							A13.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B13.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C13.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D13.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E13.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F13.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G13.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H13.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I13.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J13.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K13.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L13.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M13.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N13.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O13.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P13.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q13.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R13.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S13.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T13.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U13.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V13.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W13.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X13.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y13.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z13.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç13.SetActive (true);

						}

					} else if (x == 3) {
						string palavrateste = palavra22 [3].ToString ();
						if (palavrateste.Equals ("a")) {
							A14.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B14.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C14.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D14.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E14.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F14.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G14.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H14.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I14.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J14.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K14.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L14.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M14.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N14.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O14.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P14.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q14.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R14.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S14.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T14.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U14.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V14.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W14.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X14.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y14.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z14.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç14.SetActive (true);

						}
					} else if (x == 4) {
						string palavrateste = palavra22 [4].ToString ();
						if (palavrateste.Equals ("a")) {
							A15.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B15.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C15.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D15.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E15.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F15.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G15.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H15.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I15.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J15.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K15.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L15.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M15.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N15.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O15.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P15.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q15.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R15.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S15.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T15.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U15.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V15.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W15.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X15.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y15.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z15.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç15.SetActive (true);

						}
					} else if (x == 5) {
						string palavrateste = palavra22 [5].ToString ();
						if (palavrateste.Equals ("a")) {
							A16.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B16.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C16.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D16.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E16.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F16.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G16.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H16.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I16.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J16.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K16.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L16.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M16.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N16.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O16.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P16.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q16.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R16.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S16.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T16.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U16.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V16.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W16.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X16.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y16.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z16.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç16.SetActive (true);

						}
					} else if (x == 6) {
						string palavrateste = palavra22 [6].ToString ();
						if (palavrateste.Equals ("a")) {
							A17.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B17.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C17.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D17.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E17.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F17.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G17.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H17.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I17.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J17.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K17.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L17.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M17.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N17.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O17.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P17.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q17.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R17.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S17.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T17.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U17.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V17.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W17.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X17.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y17.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z17.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç17.SetActive (true);

						}
					} else if (x == 7) {
						string palavrateste = palavra22 [7].ToString ();
						if (palavrateste.Equals ("a")) {
							A18.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B18.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C18.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D18.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E18.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F18.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G18.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H18.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I18.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J18.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K18.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L18.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M18.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N18.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O18.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P18.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q18.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R18.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S18.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T18.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U18.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V18.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W18.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X18.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y18.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z18.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç18.SetActive (true);

						}
					} else if (x == 8) {
						string palavrateste = palavra22 [8].ToString ();
						if (palavrateste.Equals ("a")) {
							A19.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B19.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C19.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D19.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E19.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F19.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G19.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H19.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I19.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J19.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K19.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L19.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M19.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N19.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O19.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P19.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q19.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R19.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S19.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T19.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U19.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V19.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W19.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X19.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y19.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z19.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç19.SetActive (true);

						}
					} else if (x == 9) {
						string palavrateste = palavra22 [9].ToString ();
						if (palavrateste.Equals ("a")) {
							A20.SetActive (true);

						} else if (palavrateste.Equals ("b")) {
							B20.SetActive (true);

						} else if (palavrateste.Equals ("c")) {
							C20.SetActive (true);

						} else if (palavrateste.Equals ("d")) {
							D20.SetActive (true);

						} else if (palavrateste.Equals ("e")) {
							E20.SetActive (true);

						} else if (palavrateste.Equals ("f")) {
							F20.SetActive (true);

						} else if (palavrateste.Equals ("g")) {
							G20.SetActive (true);

						} else if (palavrateste.Equals ("h")) {
							H20.SetActive (true);

						} else if (palavrateste.Equals ("i")) {
							I20.SetActive (true);

						} else if (palavrateste.Equals ("j")) {
							J20.SetActive (true);

						} else if (palavrateste.Equals ("k")) {
							K20.SetActive (true);

						} else if (palavrateste.Equals ("l")) {
							L20.SetActive (true);

						} else if (palavrateste.Equals ("m")) {
							M20.SetActive (true);

						} else if (palavrateste.Equals ("n")) {
							N20.SetActive (true);

						} else if (palavrateste.Equals ("o")) {
							O20.SetActive (true);

						} else if (palavrateste.Equals ("p")) {
							P20.SetActive (true);

						} else if (palavrateste.Equals ("q")) {
							Q20.SetActive (true);

						} else if (palavrateste.Equals ("r")) {
							R20.SetActive (true);

						} else if (palavrateste.Equals ("s")) {
							S20.SetActive (true);

						} else if (palavrateste.Equals ("t")) {
							T20.SetActive (true);

						} else if (palavrateste.Equals ("u")) {
							U20.SetActive (true);

						} else if (palavrateste.Equals ("v")) {
							V20.SetActive (true);

						} else if (palavrateste.Equals ("w")) {
							W20.SetActive (true);

						} else if (palavrateste.Equals ("x")) {
							X20.SetActive (true);

						} else if (palavrateste.Equals ("y")) {
							Y20.SetActive (true);

						} else if (palavrateste.Equals ("z")) {
							Z20.SetActive (true);

						} else if (palavrateste.Equals ("ç")) {
							Ç20.SetActive (true);

						}
					} 
				}
			}
		}

		score = 0;
		Debug.Log ("score=" + score);
		if (A1.activeSelf || B1.activeSelf || C1.activeSelf || D1.activeSelf || E1.activeSelf || F1.activeSelf || G1.activeSelf || H1.activeSelf || I1.activeSelf || J1.activeSelf || K1.activeSelf || L1.activeSelf || M1.activeSelf || N1.activeSelf || O1.activeSelf || P1.activeSelf || Q1.activeSelf || R1.activeSelf || S1.activeSelf || T1.activeSelf || U1.activeSelf || V1.activeSelf || W1.activeSelf || X1.activeSelf || Y1.activeSelf || Z1.activeSelf || Ç1.activeSelf) {
			score++;
			Debug.Log ("********casa1");
			Debug.Log ("score=" + score);
		}
		if (A2.activeSelf || B2.activeSelf || C2.activeSelf || D2.activeSelf || E2.activeSelf || F2.activeSelf || G2.activeSelf || H2.activeSelf || I2.activeSelf || J2.activeSelf || K2.activeSelf || L2.activeSelf || M2.activeSelf || N2.activeSelf || O2.activeSelf || P2.activeSelf || Q2.activeSelf || R2.activeSelf || S2.activeSelf || T2.activeSelf || U2.activeSelf || V2.activeSelf || W2.activeSelf || X2.activeSelf || Y2.activeSelf || Z2.activeSelf || Ç2.activeSelf) {
			score++;
			Debug.Log ("********casa2");
			Debug.Log ("score=" + score);
		}
		if (A3.activeSelf || B3.activeSelf || C3.activeSelf || D3.activeSelf || E3.activeSelf || F3.activeSelf || G3.activeSelf || H3.activeSelf || I3.activeSelf || J3.activeSelf || K3.activeSelf || L3.activeSelf || M3.activeSelf || N3.activeSelf || O3.activeSelf || P3.activeSelf || Q3.activeSelf || R3.activeSelf || S3.activeSelf || T3.activeSelf || U3.activeSelf || V3.activeSelf || W3.activeSelf || X3.activeSelf || Y3.activeSelf || Z3.activeSelf || Ç3.activeSelf) {
			score++;
			Debug.Log ("********casa3");
			Debug.Log ("score=" + score);
		}
		if (A4.activeSelf || B4.activeSelf || C4.activeSelf || D4.activeSelf || E4.activeSelf || F4.activeSelf || G4.activeSelf || H4.activeSelf || I4.activeSelf || J4.activeSelf || K4.activeSelf || L4.activeSelf || M4.activeSelf || N4.activeSelf || O4.activeSelf || P4.activeSelf || Q4.activeSelf || R4.activeSelf || S4.activeSelf || T4.activeSelf || U4.activeSelf || V4.activeSelf || W4.activeSelf || X4.activeSelf || Y4.activeSelf || Z4.activeSelf || Ç4.activeSelf) {
			score++;
			Debug.Log ("********casa4");
			Debug.Log ("score=" + score);
		}
		if (A5.activeSelf || B5.activeSelf || C5.activeSelf || D5.activeSelf || E5.activeSelf || F5.activeSelf || G5.activeSelf || H5.activeSelf || I5.activeSelf || J5.activeSelf || K5.activeSelf || L5.activeSelf || M5.activeSelf || N5.activeSelf || O5.activeSelf || P5.activeSelf || Q5.activeSelf || R5.activeSelf || S5.activeSelf || T5.activeSelf || U5.activeSelf || V5.activeSelf || W5.activeSelf || X5.activeSelf || Y5.activeSelf || Z5.activeSelf || Ç5.activeSelf) {
			score++;
			Debug.Log ("********casa5");
			Debug.Log ("score=" + score);
		}
		if (A6.activeSelf || B6.activeSelf || C6.activeSelf || D6.activeSelf || E6.activeSelf || F6.activeSelf || G6.activeSelf || H6.activeSelf || I6.activeSelf || J6.activeSelf || K6.activeSelf || L6.activeSelf || M6.activeSelf || N6.activeSelf || O6.activeSelf || P6.activeSelf || Q6.activeSelf || R6.activeSelf || S6.activeSelf || T6.activeSelf || U6.activeSelf || V6.activeSelf || W6.activeSelf || X6.activeSelf || Y6.activeSelf || Z6.activeSelf || Ç6.activeSelf) {
			score++;
			Debug.Log ("********casa6");
			Debug.Log ("score=" + score);
		}
		if (A7.activeSelf || B7.activeSelf || C7.activeSelf || D7.activeSelf || E7.activeSelf || F7.activeSelf || G7.activeSelf || H7.activeSelf || I7.activeSelf || J7.activeSelf || K7.activeSelf || L7.activeSelf || M7.activeSelf || N7.activeSelf || O7.activeSelf || P7.activeSelf || Q7.activeSelf || R7.activeSelf || S7.activeSelf || T7.activeSelf || U7.activeSelf || V7.activeSelf || W7.activeSelf || X7.activeSelf || Y7.activeSelf || Z7.activeSelf || Ç7.activeSelf) {
			score++;
			Debug.Log ("********casa7");
			Debug.Log ("score=" + score);
		}
		if (A8.activeSelf || B8.activeSelf || C8.activeSelf || D8.activeSelf || E8.activeSelf || F8.activeSelf || G8.activeSelf || H8.activeSelf || I8.activeSelf || J8.activeSelf || K8.activeSelf || L8.activeSelf || M8.activeSelf || N8.activeSelf || O8.activeSelf || P8.activeSelf || Q8.activeSelf || R8.activeSelf || S8.activeSelf || T8.activeSelf || U8.activeSelf || V8.activeSelf || W8.activeSelf || X8.activeSelf || Y8.activeSelf || Z8.activeSelf || Ç8.activeSelf) {
			score++;
			Debug.Log ("********casa8");
			Debug.Log ("score=" + score);
		}
		if (A9.activeSelf || B9.activeSelf || C9.activeSelf || D9.activeSelf || E9.activeSelf || F9.activeSelf || G9.activeSelf || H9.activeSelf || I9.activeSelf || J9.activeSelf || K9.activeSelf || L9.activeSelf || M9.activeSelf || N9.activeSelf || O9.activeSelf || P9.activeSelf || Q9.activeSelf || R9.activeSelf || S9.activeSelf || T9.activeSelf || U9.activeSelf || V9.activeSelf || W9.activeSelf || X9.activeSelf || Y9.activeSelf || Z9.activeSelf || Ç9.activeSelf) {
			score++;
			Debug.Log ("********casa9");
			Debug.Log ("score=" + score);
		}
		if (A10.activeSelf || B10.activeSelf || C10.activeSelf || D10.activeSelf || E10.activeSelf || F10.activeSelf || G10.activeSelf || H10.activeSelf || I10.activeSelf || J10.activeSelf || K10.activeSelf || L10.activeSelf || M10.activeSelf || N10.activeSelf || O10.activeSelf || P10.activeSelf || Q10.activeSelf || R10.activeSelf || S10.activeSelf || T10.activeSelf || U10.activeSelf || V10.activeSelf || W10.activeSelf || X10.activeSelf || Y10.activeSelf || Z10.activeSelf || Ç10.activeSelf) {
			score++;
			Debug.Log ("********casa10");
			Debug.Log ("score=" + score);
		}
		if (A11.activeSelf || B11.activeSelf || C11.activeSelf || D11.activeSelf || E11.activeSelf || F11.activeSelf || G11.activeSelf || H11.activeSelf || I11.activeSelf || J11.activeSelf || K11.activeSelf || L11.activeSelf || M11.activeSelf || N11.activeSelf || O11.activeSelf || P11.activeSelf || Q11.activeSelf || R11.activeSelf || S11.activeSelf || T11.activeSelf || U11.activeSelf || V11.activeSelf || W11.activeSelf || X11.activeSelf || Y11.activeSelf || Z11.activeSelf || Ç11.activeSelf) {
			score++;
			Debug.Log ("********casa11");
			Debug.Log ("score=" + score);
		}
		if (A12.activeSelf || B12.activeSelf || C12.activeSelf || D12.activeSelf || E12.activeSelf || F12.activeSelf || G12.activeSelf || H12.activeSelf || I12.activeSelf || J12.activeSelf || K12.activeSelf || L12.activeSelf || M12.activeSelf || N12.activeSelf || O12.activeSelf || P12.activeSelf || Q12.activeSelf || R12.activeSelf || S12.activeSelf || T12.activeSelf || U12.activeSelf || V12.activeSelf || W12.activeSelf || X12.activeSelf || Y12.activeSelf || Z12.activeSelf || Ç12.activeSelf) {
			score++;
			Debug.Log ("********casa12");
			Debug.Log ("score=" + score);
		}
		if (A13.activeSelf || B13.activeSelf || C13.activeSelf || D13.activeSelf || E13.activeSelf || F13.activeSelf || G13.activeSelf || H13.activeSelf || I13.activeSelf || J13.activeSelf || K13.activeSelf || L13.activeSelf || M13.activeSelf || N13.activeSelf || O13.activeSelf || P13.activeSelf || Q13.activeSelf || R13.activeSelf || S13.activeSelf || T13.activeSelf || U13.activeSelf || V13.activeSelf || W13.activeSelf || X13.activeSelf || Y13.activeSelf || Z13.activeSelf || Ç13.activeSelf) {
			score++;
			Debug.Log ("********casa13");
			Debug.Log ("score=" + score);
		}
		if (A14.activeSelf || B14.activeSelf || C14.activeSelf || D14.activeSelf || E14.activeSelf || F14.activeSelf || G14.activeSelf || H14.activeSelf || I14.activeSelf || J14.activeSelf || K14.activeSelf || L14.activeSelf || M14.activeSelf || N14.activeSelf || O14.activeSelf || P14.activeSelf || Q14.activeSelf || R14.activeSelf || S14.activeSelf || T14.activeSelf || U14.activeSelf || V14.activeSelf || W14.activeSelf || X14.activeSelf || Y14.activeSelf || Z14.activeSelf || Ç14.activeSelf) {
			score++;
			Debug.Log ("********casa14");
			Debug.Log ("score=" + score);
		}
		if (A15.activeSelf || B15.activeSelf || C15.activeSelf || D15.activeSelf || E15.activeSelf || F15.activeSelf || G15.activeSelf || H15.activeSelf || I15.activeSelf || J15.activeSelf || K15.activeSelf || L15.activeSelf || M15.activeSelf || N15.activeSelf || O15.activeSelf || P15.activeSelf || Q15.activeSelf || R15.activeSelf || S15.activeSelf || T15.activeSelf || U15.activeSelf || V15.activeSelf || W15.activeSelf || X15.activeSelf || Y15.activeSelf || Z15.activeSelf || Ç15.activeSelf) {
			score++;
			Debug.Log ("********casa15");
			Debug.Log ("score=" + score);
		}
		if (A16.activeSelf || B16.activeSelf || C16.activeSelf || D16.activeSelf || E16.activeSelf || F16.activeSelf || G16.activeSelf || H16.activeSelf || I16.activeSelf || J16.activeSelf || K16.activeSelf || L16.activeSelf || M16.activeSelf || N16.activeSelf || O16.activeSelf || P16.activeSelf || Q16.activeSelf || R16.activeSelf || S16.activeSelf || T16.activeSelf || U16.activeSelf || V16.activeSelf || W16.activeSelf || X16.activeSelf || Y16.activeSelf || Z16.activeSelf || Ç16.activeSelf) {
			score++;
			Debug.Log ("********casa16");
			Debug.Log ("score=" + score);
		}
		if (A17.activeSelf || B17.activeSelf || C17.activeSelf || D17.activeSelf || E17.activeSelf || F17.activeSelf || G17.activeSelf || H17.activeSelf || I17.activeSelf || J17.activeSelf || K17.activeSelf || L17.activeSelf || M17.activeSelf || N17.activeSelf || O17.activeSelf || P17.activeSelf || Q17.activeSelf || R17.activeSelf || S17.activeSelf || T17.activeSelf || U17.activeSelf || V17.activeSelf || W17.activeSelf || X17.activeSelf || Y17.activeSelf || Z17.activeSelf || Ç17.activeSelf) {
			score++;
			Debug.Log ("********casa17");
			Debug.Log ("score=" + score);
		}
		if (A18.activeSelf || B18.activeSelf || C18.activeSelf || D18.activeSelf || E18.activeSelf || F18.activeSelf || G18.activeSelf || H18.activeSelf || I18.activeSelf || J18.activeSelf || K18.activeSelf || L18.activeSelf || M18.activeSelf || N18.activeSelf || O18.activeSelf || P18.activeSelf || Q18.activeSelf || R18.activeSelf || S18.activeSelf || T18.activeSelf || U18.activeSelf || V18.activeSelf || W18.activeSelf || X18.activeSelf || Y18.activeSelf || Z18.activeSelf || Ç18.activeSelf) {
			score++;
			Debug.Log ("********casa18");
			Debug.Log ("score=" + score);
		}
		if (A19.activeSelf || B19.activeSelf || C19.activeSelf || D19.activeSelf || E19.activeSelf || F19.activeSelf || G19.activeSelf || H19.activeSelf || I19.activeSelf || J19.activeSelf || K19.activeSelf || L19.activeSelf || M19.activeSelf || N19.activeSelf || O19.activeSelf || P19.activeSelf || Q19.activeSelf || R19.activeSelf || S19.activeSelf || T19.activeSelf || U19.activeSelf || V19.activeSelf || W19.activeSelf || X19.activeSelf || Y19.activeSelf || Z19.activeSelf || Ç19.activeSelf) {
			score++;
			Debug.Log ("********casa19");
			Debug.Log ("score=" + score);
		}
		if (A20.activeSelf || B20.activeSelf || C20.activeSelf || D20.activeSelf || E20.activeSelf || F20.activeSelf || G20.activeSelf || H20.activeSelf || I20.activeSelf || J20.activeSelf || K20.activeSelf || L20.activeSelf || M20.activeSelf || N20.activeSelf || O20.activeSelf || P20.activeSelf || Q20.activeSelf || R20.activeSelf || S20.activeSelf || T20.activeSelf || U20.activeSelf || V20.activeSelf || W20.activeSelf || X20.activeSelf || Y20.activeSelf || Z20.activeSelf || Ç20.activeSelf) {
			score++;
			Debug.Log ("********casa20");
			Debug.Log ("score=" + score);
		}
		Debug.Log ("total=" + total);
		Debug.Log ("scorefinal=" + score);
		if (score == total) {

			Keyboard.SetActive (false);
			ganhou.SetActive (true);
		}

	}
}