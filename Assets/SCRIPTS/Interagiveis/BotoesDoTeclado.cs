﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotoesDoTeclado : Interagiveis {

	public Text areaDeTexto;

	public override void Interagir ()
	{
		Debug.Log (name);
		GetComponent<SpriteRenderer> ().color = Color.red;
		areaDeTexto.text += name;
	}

	public override void PararDeInteragir ()
	{
		GetComponent<SpriteRenderer> ().color = new Color(0,0,0,0);
	}
}
