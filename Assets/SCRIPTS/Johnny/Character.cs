﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(UnityEngine.CharacterController))]
public class Character : Player, IPossessable
{
    [Serializable]
    public class BobSettings : object
    {
        public AnimationCurve xCurve;
        public AnimationCurve yCurve;
        [Range(0f, 10f)]
        public float walkBobScalar = 0.8f;
        [Range(0f, 10f)]
        public float runBobScalar = 1.2f;
        private float bobScalar;
        [Range(0f, 10f)]
        public float walkTimeScalar = 0.5f;
        [Range(0f, 10f)]
        public float runTimeScalar = 1.2f;
        private float timeScalar;
        float scaledTime = 0f;
       
        public Vector3 Update(float time, float scalar)
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                bobScalar = Mathf.Lerp(bobScalar, runBobScalar, time * 5f);
                timeScalar = Mathf.Lerp(timeScalar, runTimeScalar, time * 5f);
            }
            else
            {
                bobScalar = Mathf.Lerp(bobScalar, walkBobScalar, time * 5f);
                timeScalar = Mathf.Lerp(timeScalar, walkTimeScalar, time * 5f);
            }

            scaledTime += time * timeScalar;

            return new Vector3(xCurve.Evaluate(scaledTime) * bobScalar * scalar, yCurve.Evaluate(scaledTime) * bobScalar * scalar);
        }
    }

    [Header("Actor")]
    public bool                             autoPossess = false;
    [Header("Move Settings")]
    [Range(0.01f, 10f)]
    public float                            walkSpeed = 2f;
    [Range(0.01f, 10f)]
    public float                            runSpeed = 5f;
    [Header("Camera Settings")]
    public Transform                        cameraPivot;
    public Vector2                          minMaxY = new Vector2(-80, 80);
    public float                            lookSpeed = 2;
    public BobSettings                      bobSettings;
    private UnityEngine.CharacterController controller;
    public Transform                        grabPivot;


    protected override void Start()
    {
        base.Start();

        Singleton = this;

        if (autoPossess)
        {
            PlayerController[] playerControllers = GameObject.FindObjectsOfType<PlayerController>();
            if (playerControllers.Length > 0)
                playerControllers[0].Possess(this);
        }

        controller = GetComponent<UnityEngine.CharacterController>();
    }

    public void Move(Vector3 motion, bool headBob = false)
    {
        controller.Move(motion);

        if (!headBob)
            return;

        cam.transform.localPosition = Vector3.zero;
        cam.transform.localPosition = bobSettings.Update(Time.fixedDeltaTime, motion.normalized.sqrMagnitude);
    }

    protected override void OnPossessed(PlayerController playerController)
    {
        base.OnPossessed(playerController);

        GameInstance.GameState.EnableCursor(false, CursorLockMode.Locked);
        cam.gameObject.SetActive(true);
    }

    protected override void OnUnpossess(PlayerController playerController)
    {
        base.OnUnpossess(playerController);

        cam.gameObject.SetActive(false);
    }

    public static Character Singleton { get; private set; }
}
