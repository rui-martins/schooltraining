﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController2 : MonoBehaviour
{
    public enum MouseButton
    {
        left = 0,
        right = 1,
        middle = 2
    }

    public delegate void InteractableEventHandler(CharacterController2 sender, Interactable interactable);

    public event InteractableEventHandler OnStartInteraction,
                                            OnStopInteraction;

    Character character;
    public MouseButton mouseButton = 0;
    [Header("Interactability")]
    public LayerMask layerMask;
    public float maxDistance = 5f;
    private Interactable lastInteractable,
                                interactable;
    private float yRotation;

    private void Update()
    {

        interactable = GetInteractableFromRay();
        if (interactable != null)
            interactable.SpamOutline();

        if (Input.GetMouseButtonDown((int)mouseButton) && !GameInstance.GameState.Paused)
            Interact();
        if (Input.GetMouseButtonUp((int)mouseButton) && !GameInstance.GameState.Paused)
            StopInteract();

        if (character != null && !GameInstance.GameState.Paused)
            Look();

        if (GameInstance.GameState.Paused)
            GameInstance.HUD.EnableInteractMessage(null);
        else
        {
            if (interactable != null)
                interactable.OnHover();

            GameInstance.HUD.EnableInteractMessage(interactable);
        }
    }

    private void FixedUpdate()
    {

        if (character == null)
            return;

        Walk();
    }

    private void Walk()
    {
        float forward = Input.GetAxis("Vertical");
        float sides = Input.GetAxis("Horizontal");

        Vector3 dir = character.transform.forward * forward;
        dir += character.transform.right * sides;

        float moveSpeed = character.walkSpeed;

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            moveSpeed = character.runSpeed;

        character.Move(dir * moveSpeed * Time.deltaTime, true);
        character.Move(Physics.gravity * Time.deltaTime, false);
    }

    private void Look()
    {
        if (character == null)
            return;

        Vector3 rotation = character.transform.eulerAngles;
        rotation.y += Input.GetAxis("Mouse X") * character.lookSpeed;
        character.transform.eulerAngles = rotation;

        yRotation += Input.GetAxis("Mouse Y") * character.lookSpeed;
        yRotation = Mathf.Clamp(yRotation, character.minMaxY.x, character.minMaxY.y);
        character.cam.transform.localEulerAngles = new Vector3(-yRotation, character.cam.transform.localEulerAngles.y, 0);
    }

    private void Interact()
    {
        if (interactable != null && !interactable.active)
            return;

        //garants always release a interactable and 
        //not hold two at same time
        ReleaseLastInteractable();

        //try to find interactable
        lastInteractable = interactable;
        if (lastInteractable != null)
        {
            //interact
            lastInteractable.OnStartInteract();
            if (OnStartInteraction != null)
                OnStartInteraction(this, lastInteractable);
        }
    }

    private void ReleaseLastInteractable()
    {
        if (lastInteractable != null)
        {
            lastInteractable.OnStopInteract(false);
            lastInteractable = null;
        }
    }

    private void StopInteract()
    {
        Interactable pointingInteractable = GetInteractableFromRay();
        if (lastInteractable != null)
        {
            //check if still pointing to last interactable
            if (lastInteractable == pointingInteractable)
            {
                //stop interact (pointing to)
                lastInteractable.OnStopInteract(true);
            }
            else
            {
                //stop interact (not pointing to)
                lastInteractable.OnStopInteract(false);
            }

            if (OnStopInteraction != null)
                OnStopInteraction(this, lastInteractable);

            lastInteractable = null;
        }

    }

    private Interactable GetInteractableFromRay()
    {
        Interactable interactable = null;
        Ray ray = new Ray();

        if (character != null)
            ray = new Ray(character.cam.transform.position, character.cam.transform.forward);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, maxDistance, ~(1 << 2)))
            interactable = hit.transform.GetComponent<Interactable>();

        return interactable;
    }
}