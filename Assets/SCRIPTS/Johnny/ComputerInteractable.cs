﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerInteractable : Interactable
{
    public enum ENUM_AviaoScene : ushort
    {
        Abecedario = 0,
        AEIOU = 1,
        NUM = 2,
    }

    public ENUM_AviaoScene scene = ENUM_AviaoScene.Abecedario;

    private void Start()
    {
        switch(scene)
        {
            case ENUM_AviaoScene.Abecedario:
                interactMessage = "Jogar Abecedário";
                break;
            case ENUM_AviaoScene.AEIOU:
                interactMessage = "Jogar AEIOU";
                break;
            case ENUM_AviaoScene.NUM:
                interactMessage = "Jogar Números";
                break;
        }
    }

    public override void OnStartInteract()
    {
        base.OnStartInteract();

        switch(scene)
        {
            case ENUM_AviaoScene.Abecedario:
                Application.LoadLevel("AircraftJet2Axis");
                break;
            case ENUM_AviaoScene.AEIOU:
                Application.LoadLevel("AircraftJet2AxisAEI");
                break;
            case ENUM_AviaoScene.NUM:
                Application.LoadLevel("AircraftJet2AxisNUM");
                break;
        }
        
    }
}
