﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryLevelInstance : LevelInstance
{
    protected override void Start()
    {
        base.Start();
        
        GameInstance.HUD.EnableEntryPanel(true);
    }
}
