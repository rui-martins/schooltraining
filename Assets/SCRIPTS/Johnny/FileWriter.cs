﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileWriter : MonoBehaviour
{
    public static string dataPath;

    private void Awake()
    {
        dataPath = Application.dataPath + "/Experiências";

        if (!Directory.Exists(dataPath))
            Directory.CreateDirectory(dataPath);
    }

    public static void CreateSubDirectory(string name)
    {
        if (!Directory.Exists(dataPath + "\\" + name))
            Directory.CreateDirectory(dataPath + "\\" + name);
    }

    public static StreamWriter WriteNewFile(string name)
    {
        return new StreamWriter(dataPath + "/" + name);
    }

    public static StreamWriter OpenFile(String name)
    {
        return new StreamWriter(dataPath + "/" + name, true);
    }

    public static bool FileExists(string name)
    {
        return File.Exists(dataPath + "/" + name);
    }

    public static void CloseFile(StreamWriter writer)
    {
        writer.Close();
        writer.Dispose();
    }

    public static void WriteData(StreamWriter writer, string data)
    {
        writer.WriteLine(data);
    }
}