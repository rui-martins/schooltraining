﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameInstance : MonoBehaviour
{
    public GameObject playerController;
    [HideInInspector]
    public string path;

    private void Awake()
    {
        if (Singleton != null)
            Destroy(gameObject);

        DontDestroyOnLoad(this);
        Singleton   = this;
        GameState   = GetComponentInChildren<GameState>();
        HUD         = GetComponentInChildren<HUD>();

        if (playerController != null)
            Instantiate(playerController);
    }

    private void Start()
    {
    }

    public void CreateStudentDirectory(string studentName)
    {
        FileWriter.CreateSubDirectory(studentName);
    }

    public void CreateStudentFiles(string studentName)
    {
        FileWriter.CreateSubDirectory(studentName);
        if (!FileWriter.FileExists(studentName + "\\Sopa.txt"))
        {
            StreamWriter stream = FileWriter.WriteNewFile(studentName + "\\Sopa.txt");
            FileWriter.CloseFile(stream);
        }
        if (!FileWriter.FileExists(studentName + "\\Pavilhao.txt"))
        {
            StreamWriter stream = FileWriter.WriteNewFile(studentName + "\\Pavilhao.txt");
            FileWriter.CloseFile(stream);
        }
    }

    public static void LoadScene(string sceneName)
    {
        Singleton.StartCoroutine(Singleton.LoadLevel(sceneName));   
    }

    IEnumerator LoadLevel(string sceneName)
    {
        //HUD.ShowLoadingScreen(true);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        Application.LoadLevel(sceneName);
        //HUD.ShowLoadingScreen(false);
    }

    public static PlayerController[] PlayerControllers { get { return GameObject.FindObjectsOfType<PlayerController>(); } }

    public static GameInstance Singleton        { get; private set; }
    public static GameState GameState           { get; private set; }
    public static HUD HUD                       { get; private set; }
}
