﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public delegate void EventHandler(HUD sender);

    public event EventHandler OnMoneyChanged;

    public GameObject interactableMessageObject;
    public Text interactableMessage;
    public GameObject entryPanel;
    public Text studentName;
    public Text completedTaskMessage;
    public GameObject chemestryPanel;
    public GameObject electroPanel;

    public void EnableInteractMessage(Interactable interactable)
    {
        if (interactableMessage != null)
        {
            if (interactable != null && interactable.show)
            {
                interactableMessage.text = interactable.interactMessage;
                interactableMessage.color = interactable.active ? Color.green : Color.red;
            }
            else
                interactableMessage.text = "";
        }

        if (interactableMessageObject != null)
            interactableMessageObject.SetActive(interactable != null && interactable.show ? true : false);
    }

    public void EnableElectroPanel (bool enable)
    {
        GameInstance.GameState.Paused = enable;
        electroPanel.SetActive(enable);
    }

    public void EnableEntryPanel(bool enable)
    {
        if (string.IsNullOrEmpty(studentName.text))
            return;

        if (entryPanel == null)
            return;

        entryPanel.SetActive(enable);
    }

    public void EnableChemestryMenu(bool enable)
    {
        GameInstance.GameState.Paused = enable;
        chemestryPanel.SetActive(enable);
    }

    public void StartChemestryGame(int dif)
    {
        TableInteractive[] table = GameObject.FindObjectsOfType<TableInteractive>();

        table[dif].StartGame();

        table[dif].entities.SetActive(true);
        table[dif].door.ShutDoor();
    }

    public void StartElectroGame(int difficulty)
    {
        AmpController controller = ((SchoolLevelInstance)LevelInstance.Singleton).ampController;
        controller.difficulty = difficulty;
        controller.gameObject.SetActive(true);

        GameInstance.GameState.Paused = false;
        GameInstance.PlayerControllers[0].Possess(controller);
    }

    public void StartGame()
    {
        if (string.IsNullOrEmpty(studentName.text))
            return;

        GameInstance.Singleton.CreateStudentDirectory(studentName.text);
        GameInstance.Singleton.CreateStudentFiles(studentName.text);

        UnityEngine.SceneManagement.SceneManager.LoadScene("SchoolSceneDay");
    }

    public void EnableTaskCompleted(bool enable)
    {
        completedTaskMessage.gameObject.SetActive(enable);
    }
}