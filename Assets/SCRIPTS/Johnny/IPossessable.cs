﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPossessable
{
    void Possess(PlayerController playerController);
    void Unpossess(PlayerController playerController);
}
