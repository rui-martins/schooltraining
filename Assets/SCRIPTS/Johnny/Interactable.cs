﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    public delegate void EventHandler(Interactable sender);

    public event EventHandler   OnStartInteraction,
                                OnStopInteraction;

    List<Material> outlinedMaterials = new List<Material>();
    float outlineWidth = 0f;
    public bool active  = true,
                show    = true;
    public string interactMessage = "Interact";
    
    protected virtual void Awake()
    {
        Renderer r = GetComponent<Renderer>();
        if (r != null)
            outlinedMaterials.AddRange(r.materials);

        Renderer[] rs = GetComponentsInChildren<Renderer>();
        for (int i = 0; i < rs.Length; i++)
            outlinedMaterials.AddRange(rs[i].materials);
    }

    protected virtual void Update()
    {
        outlineWidth = Mathf.Lerp(outlineWidth, 0f, Time.deltaTime * 10f);
        for (int i = 0; i < outlinedMaterials.Count; i++)
        {
            outlinedMaterials[i].SetFloat("_Outline", outlineWidth);
            outlinedMaterials[i].SetColor("_OutlineColor", active ? Color.green : Color.red);
        }
    }

    public void SpamOutline()
    {
        outlineWidth = 1f;
    }

    public virtual void OnHover()
    {

    }

    public virtual void OnStartInteract()
    {
        if (!active)
            return;

        if (OnStartInteraction != null)
            OnStartInteraction(this);
    }

    public virtual void OnStopInteract(bool over)
    {
        if (!active)
            return;

        if (OnStopInteraction != null)
            OnStopInteraction(this);
    }
}