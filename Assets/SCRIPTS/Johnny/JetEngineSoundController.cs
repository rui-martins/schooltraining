﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Aeroplane;

[RequireComponent(typeof(AudioSource))]
public class JetEngineSoundController : MonoBehaviour
{
    AudioSource audioSource;
    public AeroplaneController controller;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }


    private void LateUpdate()
    {
        audioSource.pitch = Mathf.Lerp(audioSource.pitch, 0.6f + controller.MaxEnginePower / 50f, Time.deltaTime);
    }
}
