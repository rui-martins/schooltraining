﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class PavilhaoInteractable : Interactable
{
    public enum ENUM_Level
    {
        Cores = 0,
        Cores_Formato = 1,
        Formato = 2,
        Cores_Futebol = 3,
        Cores_Formato_Futebol = 4,
        Formato_Futebol = 5,
    }

    public ENUM_Level levelToLoad;

    public override void OnStartInteract()
    {
        base.OnStartInteract();

        StreamWriter writer = FileWriter.OpenFile(GameInstance.HUD.studentName.text + "\\Pavilhao.txt");

        switch (levelToLoad)
        {
            case ENUM_Level.Cores:
                FileWriter.WriteData(writer, "\nNova Experiência Por Cores : " + System.DateTime.UtcNow.ToString());
                ((SchoolLevelInstance)LevelInstance.Singleton).StartObjectFix("ObjectFix_ByColor");
                break;
            case ENUM_Level.Cores_Formato:
                FileWriter.WriteData(writer, "\nNova Experiência Por Cores e Formato : " + System.DateTime.UtcNow.ToString());
                ((SchoolLevelInstance)LevelInstance.Singleton).StartObjectFix("ObjectFix_ByColorAndFormat");
                break;
            case ENUM_Level.Formato:
                FileWriter.WriteData(writer, "\nNova Experiência Por Formato : " + System.DateTime.UtcNow.ToString());
                ((SchoolLevelInstance)LevelInstance.Singleton).StartObjectFix("ObjectFix_ByFormat");
                break;

            case ENUM_Level.Cores_Futebol:
                FileWriter.WriteData(writer, "\nNova Experiência Por Cores : " + System.DateTime.UtcNow.ToString());
                ((SchoolLevelInstance)LevelInstance.Singleton).StartObjectFix("ObjectFix_ByColor_Soccer");
                break;
            case ENUM_Level.Cores_Formato_Futebol:
                FileWriter.WriteData(writer, "\nNova Experiência Por Cores e Formato : " + System.DateTime.UtcNow.ToString());
                ((SchoolLevelInstance)LevelInstance.Singleton).StartObjectFix("ObjectFix_ByColorAndFormat_Soccer");
                break;
            case ENUM_Level.Formato_Futebol:
                FileWriter.WriteData(writer, "\nNova Experiência Por Formato : " + System.DateTime.UtcNow.ToString());
                ((SchoolLevelInstance)LevelInstance.Singleton).StartObjectFix("ObjectFix_ByFormat_Soccer");
                break;
        }

        FileWriter.CloseFile(writer);
    }
}
