﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Actor
{
    public Camera cam;

    protected override void OnPossessed(PlayerController playerController)
    {
        base.OnPossessed(playerController);

        if (cam != null)
            cam.enabled = true;
    }

    protected override void OnUnpossess(PlayerController playerController)
    {
        base.OnUnpossess(playerController);

        if (cam != null)
            cam.enabled = false;
    }
}
