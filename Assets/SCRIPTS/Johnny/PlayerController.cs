﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class PlayerController : Actor
{
    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }

    public void Possess(Actor actor)
    {
        if (PossessedActor != null)
            Unpossess();

        PossessedActor = actor;
        ((IPossessable)actor).Possess(this);

        OnPossess(actor);
    }

    public void Unpossess()
    {
        if (PossessedActor == null)
            return;

        OnUnpossess(PossessedActor);

        ((IPossessable)PossessedActor).Unpossess(this);
        PossessedActor = null;
    }

    protected virtual void OnPossess(Actor actor)
    {

    }

    protected virtual void OnUnpossess(Actor actor)
    {

    }

    public static Actor PossessedActor { get; private set; }
}
