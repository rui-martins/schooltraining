﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SopaLevelInstance : LevelInstance
{
    public Transform words;

    public void EnableWords(bool value)
    {
       foreach(Transform t in words)
            t.gameObject.GetComponent<MeshRenderer>().enabled = value;
    }
}
