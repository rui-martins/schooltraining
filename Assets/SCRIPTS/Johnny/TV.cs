﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class TV : MonoBehaviour
{
    public delegate void EventHandler(TV sender);

    bool on = false;
    private VideoPlayer videoPlayer;
    [SerializeField]
    private VideoClip[] clips;
    private int currentChannel = 0;
    private RawImage _videoTexture;

    public event EventHandler OnTvToggleOnOff;

    private void Start()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        _videoTexture = GetComponentInChildren<RawImage>();
        _videoTexture.enabled = false;
        videoPlayer.clip = clips[0];
        videoPlayer.SetTargetAudioSource(0, GetComponent<AudioSource>());

        GameInstance.GameState.OnPausedChanged += OnPausedChanged;
    }

    public void ToggleOnOff()
    {
        on = !on;

        if (on)
        {
            _videoTexture.enabled = true;
            videoPlayer.Play();
        }
        else
        {
            videoPlayer.Pause();
            _videoTexture.enabled = false;
        }

        if (OnTvToggleOnOff != null)
            OnTvToggleOnOff(this);
    }

    public void PreviousChannel()
    {
        if (currentChannel > 0)
        {
            currentChannel--;
            videoPlayer.clip = clips[currentChannel];
            videoPlayer.Play();
        }
    }

    public void NextChannel()
    {
        if (currentChannel < clips.Length - 1)
        {
            currentChannel++;
            videoPlayer.clip = clips[currentChannel];
            videoPlayer.Play();
        }
    }

    private void OnPausedChanged(GameState sender)
    {
        if (sender.Paused)
        {
            if (on)
                videoPlayer.Pause();
        }
        else if (on)
            videoPlayer.Play();

    }

    public bool IsOn { get { return on; } }
}
