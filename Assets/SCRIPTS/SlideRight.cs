﻿using UnityEngine;
using System.Collections;

public class SlideRight : MonoBehaviour
{
    
    // Use this for initialization
    public float doorStart;
    public float doorClosed;
    public float doorOpen;
    public float doorOpenSpeed;
    public float doorShutSpeed;
    public float doorFinalOpen;
    public int status;
    
    
    public void Start()
    {
        doorFinalOpen = 0.0f;
        doorStart = 1.162791f;
        doorClosed = 0.0f;
        doorOpen = 9.862792f;
        doorOpenSpeed = 0.1f;
        doorShutSpeed = -0.1f;
       
        
        
        // 0 closed, 1 opened

    }


    // Update is called once per frame
    public void Update()
    {
        doorClosed = transform.position.x;
        doorFinalOpen = transform.position.x;
        
        if ( status == 1 && doorClosed < doorOpen) { 
            transform.Translate(doorOpenSpeed, 0, 0);
            
        }
        
        if (status == 0 && doorClosed > doorStart)
        {
            transform.Translate(-0.1f, 0, 0 );
            
        }

    }
}
