﻿using System;

public static class WriteTxt
{
    ///// writen by Roberto Gomes - junior.rmg.rj@gmail.com


    public static string desktopFolder { get { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }}
    public static string myDocumentsFolder { get { return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); } }
    public static string instalationFolder { get { return Environment.CurrentDirectory; } }

    public static void Write(string txtAdrres, string txtName, string txt, bool eraseDocumentText)
    {
        string i = txtAdrres + @"\" + txtName + ".txt";

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(i, !eraseDocumentText))
        {
            file.Write(txt);
        }

    }

    public static void WriteNewLine(string txtAdrres, string txtName, string txt, bool eraseDocumentText)
    {
        string i = txtAdrres + @"\" + txtName + ".txt";

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(i, !eraseDocumentText))
        {
            file.WriteLine(txt);
        }

    }

    public static void WriteHeader(string txtAdrres, string txtName, string[] txt, bool eraseDocumentText)
    {
        string i = txtAdrres + @"\" + txtName + ".txt";

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(i, !eraseDocumentText))
        {
            for (int j= 0; j < txt.Length; j++)
            {
                file.WriteLine(txt[j]);
            }
        }

    }

    public static void EraseContent(string txtAdrres, string txtName)
    {
        string i = txtAdrres + @"\" + txtName + ".txt";

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(i, false))
        {
            file.Write("");
        }

    }

}