﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoltTest
{
    public class ChangeColorOnMouseOver : MonoBehaviour
    {
        AmpHandlesController handleParent;
        GameManager gameMan;

        public GameObject buttonTop;

        void Start()
        {
            gameMan = GameObject.Find("_GameManager").GetComponent<GameManager>();
            handleParent = GetComponentInParent<AmpHandlesController>();
        }

        void Update()
        {
            if (!gameMan.canClick)
            {
                buttonTop.GetComponent<Renderer>().material.color = handleParent.normalColor;
            }
        }

        void OnMouseEnter()
        {
            if (gameMan.canClick)
            {
                buttonTop.GetComponent<Renderer>().material.color = handleParent.mouseOverColor;
            }
        }

        void OnMouseExit()
        {
            if (gameMan.canClick)
            {
                buttonTop.GetComponent<Renderer>().material.color = handleParent.normalColor;
            }
        }
    }
}