﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace VoltTest
{
    public class GameManager : MonoBehaviour
    {
        public bool canClick;
        public GameObject handles1;
        public GameObject handles2;
        public Animator pendulum;
        bool normalPos = false;

        public int easyCount;
        public int hardCount;
        [Range(1.5f, 3f)] public float hardScale = 2f;
        private int count;
        private int maxCount;

        public GameObject startPanel;
        public GameObject endPanel;        

        public enum Dificulty
        {
            easy,
            hard
        }
        public Dificulty dificulty;

        void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            canClick = false;
            startPanel.SetActive(true);
            endPanel.SetActive(false);
        }

        void Update()
        {
            if (canClick)
            {
                if (Input.GetMouseButtonDown(0))
                    RunRayCast();
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                SwitchHandles();
            }
        }

        void StartLevel()
        {
            handles1.SetActive(true);
            handles2.SetActive(false);
            canClick = true;
            RandomizeSwing();
            SetDificulty();
        }

        void RunRayCast()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            GameObject parentObject;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.transform.parent.tag == "Handle")
                {
                    parentObject = hit.collider.transform.parent.gameObject;

                    parentObject.GetComponentInChildren<PlayPressAnimation>().PlayAnimation();
                    parentObject.GetComponentInChildren<PlayPressAnimation>().PlaySound();

                    canClick = false;

                    if (count < maxCount)
                        StartCoroutine(ResetEverything());
                    else
                        endPanel.SetActive(true);
                }
            }
        }

        void SwitchHandles()
        {
            normalPos = !normalPos;

            if (!normalPos)
            {
                handles1.SetActive(true);
                handles2.SetActive(false);
            }
            else
            {
                handles1.SetActive(false);
                handles2.SetActive(true);
            }
        }

        void RandomizeSwing()
        {
            int var;

            var = Random.Range(1, 11); // Random.Range exculdes the last number, this randomization goes from 1-10

            if (var % 2 == 0)
                pendulum.GetComponent<PendulumAnimator>().SwingRight();
            else
                pendulum.GetComponent<PendulumAnimator>().SwingLeft();
                
            count++;
        }

        void SetDificulty()
        {
            switch (dificulty)
            {
                case Dificulty.easy:
                    Time.timeScale = 1;
                    maxCount = easyCount;
                    break;

                case Dificulty.hard:
                    Time.timeScale = hardScale;
                    maxCount = hardCount;
                    break;
            }
        }

        public void StartEasy()
        {
            dificulty = Dificulty.easy;
            startPanel.SetActive(false);
            StartLevel();
        }

        public void StartHard()
        {
            dificulty = Dificulty.hard;
            startPanel.SetActive(false);
            StartLevel();
        }

        public void EndLevel()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(1);
            
        }

        IEnumerator ResetEverything()
        {
            yield return new WaitForSeconds(2);
            canClick = true;
            RandomizeSwing();
        }
    }
}