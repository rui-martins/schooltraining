﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoltTest
{
    public class PlayPressAnimation : MonoBehaviour
    {
        Animator anim;

        private void Start()
        {
            anim = GetComponent<Animator>();
        }

        public void PlayAnimation()
        {
            anim.SetTrigger("Press");
        }

        public void PlaySound()
        {
            GetComponentInParent<AudioSource>().Play();
        }
    }
}