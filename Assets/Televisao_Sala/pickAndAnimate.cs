﻿using UnityEngine;
using System.Collections;

public class pickAndAnimate : MonoBehaviour
{
	public Transform spawn, camera;
    
	public bool move = false;

	// Use this for initialization
	void Start ()
	{
        
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(move)
			gameObject.transform.position = spawn.position;
	}

	void OnMouseDown ()
	{
        transform.position = spawn.position;
        transform.parent = camera.transform;
        
        if(this.name == "mug")
            GetComponent<Animator>().Play("drink");
        else if(this.name == "escova")
            GetComponent<Animator>().Play("brush");

        StartCoroutine(Reset());
	}

	IEnumerator Reset ()
	{
		yield return new WaitForSeconds(3);
        
        Destroy(this.gameObject);
	}
}